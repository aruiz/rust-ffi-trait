use std::os::raw::c_char;
use std::ffi::CString;

pub struct A {
  name: CString
}

pub struct B {
  anothername: CString
}
pub trait T {
  fn get_name(&self) -> &CString;
}

impl T for A {
  fn get_name(&self) -> &CString {
    &self.name
  }
}

impl T for B {
  fn get_name(&self) -> &CString {
    &self.anothername
  }
}

#[no_mangle]
pub extern "C" fn a_new() -> *mut A {
  let a = Box::new(A {name: CString::new("a somename").expect("yikes")});
  Box::into_raw(a)
}

#[no_mangle]
pub extern "C" fn b_new() -> *mut B {
  let b = Box::new(B {anothername: CString::new("b somename").expect("yikes")});
  Box::into_raw(b)
}

#[repr(C)]
pub struct TC {
  data: *mut std::os::raw::c_void,
  vtable: *mut std::os::raw::c_void
}

#[no_mangle]
pub extern "C" fn a_to_t (a: *mut A) -> TC {
  let mut a = unsafe { Box::from_raw(a) };
  let ret: TC = {
    let aref: &mut dyn T = &mut *a;
    unsafe { std::mem::transmute::<&mut dyn T,TC> (aref) }
  };
  Box::into_raw(a);
  ret
}

#[no_mangle]
pub extern "C" fn b_to_t (b: *mut B) -> TC {
  let mut b = unsafe { Box::from_raw(b) };
  let ret: TC = {
    let aref: &mut dyn T = &mut *b;
    unsafe { std::mem::transmute::<&mut dyn T,TC> (aref) }
  };
  Box::into_raw(b);
  ret
}

#[no_mangle]
pub extern "C" fn a_drop(aself: *mut A) {
  unsafe {Box::from_raw(aself)};
}

#[no_mangle]
pub extern "C" fn b_drop(bself: *mut B) {
  unsafe {Box::from_raw(bself)};
}

#[no_mangle]
pub extern "C" fn t_get_name(tself: TC) -> *const c_char {
  let tself = unsafe { std::mem::transmute::<TC,&mut dyn T> (tself)};
  let name = tself.get_name();
  name.as_c_str().as_ptr()
}

/*
// C MAIN: THIS SEGFAULTS
#include <stdio.h>

typedef struct {} A;
typedef struct {} B;
typedef struct {} T;

A* a_new();
B* b_new();
T* a_to_t(A*);
T* b_to_t(B*);
char* t_get_name(T*);

void a_drop(A*);
void b_drop(B*);

int main () {
  A* a = a_new();
  B* b = b_new();
  T* ta = a_to_t(a);
  T* tb = b_to_t(b);

  t_get_name(tb);
  t_get_name(ta);
  a_drop(a);
  b_drop(b);
}
}*/
